package com.timbuchalkacourse.section7.EncapsulationExercise38;

public class Main {

    public static void main(String[] args) {
        Printer printer = new Printer(50, true);

        System.out.println(printer.printPages(0));
        System.out.println(printer.printPages(3));
        System.out.println(printer.getPagesPrinted());

    }
}
