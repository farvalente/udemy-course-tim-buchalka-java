package com.timbuchalkacourse.section7.CompositionChallengeVideo;

public class Bed {

    private String bedType;
    private double width;
    private double length;

    public Bed(String bedType, double width, double length) {
        this.bedType = bedType;
        this.width = width;
        this.length = length;
    }

    public double getBedDimension(){
        return width * length;
    }

}
