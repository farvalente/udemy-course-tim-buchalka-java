package com.timbuchalkacourse.section7.CompositionChallengeVideo;

public class Lamp {

    private String lampType;
    private int numberOfLights;
    private boolean switchOnOff;

    public Lamp(String lampType, int numberOfLights) {
        this.lampType = lampType;
        this.numberOfLights = numberOfLights;
    }

    public int getNumberOfLights() {
        return numberOfLights;
    }

    public void switchOnOff(){
        if(switchOnOff){
            switchOnOff = false;
            System.out.println("Switched lamp off");
        } else {
            switchOnOff = true;
            System.out.println("Switched lamp on");
        }
    }
}
