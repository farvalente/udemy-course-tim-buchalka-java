package com.timbuchalkacourse.section7.CompositionChallengeVideo;

public class Wall {

    private int width;
    private int height;
    private boolean isPainted;
    private boolean hasWallpaper;
    private String color;

    public Wall(int width, int height, boolean isPainted, boolean hasWallpaper) {
        this.width = width;
        this.height = height;
        this.isPainted = isPainted;
        this.hasWallpaper = hasWallpaper;
    }

    public void setPaintColor(String paintColor) {
        this.color = paintColor;
        System.out.println("Painted the walls of " + color);
    }

    public String getColor() {
        return color;
    }

    public int getWallArea(){
        return width * height;
    }
}
