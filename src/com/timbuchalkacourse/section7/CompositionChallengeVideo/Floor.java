package com.timbuchalkacourse.section7.CompositionChallengeVideo;

public class Floor {

    private String floorType;
    private int width;
    private int length;

    public Floor(String floorType, int width, int length) {
        this.floorType = floorType;
        this.width = width;
        this.length = length;
    }

    public int getFloorArea(){
        return width * length;
    }

}
