package com.timbuchalkacourse.section7.CompositionChallengeVideo;

public class Main {

    public static void main(String[] args) {
        Lamp lamp = new Lamp("Lava Lamp", 1);
        Ceiling ceiling = new Ceiling(5,5, "white");
        Floor floor = new Floor("wood", 5, 5);
        Bed bed = new Bed("double", 1.8, 2.2);
        Wall wall1 = new Wall(5, 3, true, false);
        Wall wall2 = new Wall(5, 3, true, false);
        Wall wall3 = new Wall(5, 3, true, false);
        Wall wall4 = new Wall(5, 3, true, false);

        Bedroom bedroom = new Bedroom("Xico", wall1, wall2, wall3, wall4, ceiling, floor, bed, lamp);

        bedroom.getLamp().switchOnOff();
        bedroom.setWallsColor("white");

    }
}
