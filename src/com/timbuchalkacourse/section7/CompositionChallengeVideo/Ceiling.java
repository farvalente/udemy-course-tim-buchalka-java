package com.timbuchalkacourse.section7.CompositionChallengeVideo;

public class Ceiling {

    private int width;
    private int length;
    private String paintedColor;

    public Ceiling(int width, int length, String paintedColor) {
        this.width = width;
        this.length = length;
        this.paintedColor = paintedColor;
    }

    public int getCeilingArea(){
        return width * length;
    }
}
