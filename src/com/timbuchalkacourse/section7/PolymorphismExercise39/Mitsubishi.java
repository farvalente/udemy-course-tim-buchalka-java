package com.timbuchalkacourse.section7.PolymorphismExercise39;

public class Mitsubishi extends Car {

    public Mitsubishi(int cylinder, String name) {
        super(cylinder, name);
    }

    @Override
    public String startEngine() {
        return "Mitsubishi starts engine";
    }

    @Override
    public String accelerating() {
        return "Mitsubishi accelerates";
    }

    @Override
    public String brake() {
        return "Mitsubishi brakes";
    }
}
