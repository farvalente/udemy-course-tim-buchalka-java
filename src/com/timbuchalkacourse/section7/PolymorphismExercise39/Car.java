package com.timbuchalkacourse.section7.PolymorphismExercise39;

public class Car {

    private boolean engine;
    private int cylinder;
    private String name;
    private int wheels;

    public Car(int cylinder, String name) {
        this.cylinder = cylinder;
        this.name = name;
        this.engine = true;
        this.wheels = 4;
    }

    public String startEngine(){
        return "Car Engine is starting";
    }

    public String accelerating(){
        return "Car is accelerating";
    }

    public String brake(){
        return "Car is breaking";
    }

    public int getCylinder() {
        return cylinder;
    }

    public String getName() {
        return name;
    }
}
