package com.timbuchalkacourse.section7.PolymorphismExercise39;

public class Ford extends Car {

    public Ford(int cylinder, String name) {
        super(cylinder, name);
    }

    @Override
    public String startEngine() {
        return "Ford starts engine";
    }

    @Override
    public String accelerating() {
        return "Ford accelerates";
    }

    @Override
    public String brake() {
        return "Ford brakes";
    }
}
