package com.timbuchalkacourse.section7.PolymorphismExercise39;

public class Holden extends Car {

    public Holden(int cylinder, String name) {
        super(cylinder, name);
    }

    @Override
    public String startEngine() {
        return "Holden starts engine";
    }

    @Override
    public String accelerating() {
        return "Holden accelerates";
    }

    @Override
    public String brake() {
        return "Holden brakes";
    }
}
