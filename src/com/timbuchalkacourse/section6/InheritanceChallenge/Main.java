package com.timbuchalkacourse.section6.InheritanceChallenge;

public class Main {

    public static void main(String[] args) {
        Mustang mustang = new Mustang(36);

        mustang.steer(45);
        mustang.accelerate(30);
    }

}
