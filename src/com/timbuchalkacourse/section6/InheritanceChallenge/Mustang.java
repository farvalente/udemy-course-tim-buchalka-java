package com.timbuchalkacourse.section6.InheritanceChallenge;

public class Mustang extends Car {

    private int roadServiceMonths;

    public Mustang(int roadServiceMonths) {
        super("Mustang", "Sport", 4, 2, 6, false);
        this.roadServiceMonths = roadServiceMonths;
    }


    /**
     *
     * @param rate if positive, means it's accelerating, if negative it's decelerating
     *
     *            this is just na example method, it's incomplete
     */
    public void accelerate(int rate) {

        int newVelocity = getCurrentSpeed() + rate;
        if (newVelocity == 0) {
            stop();
            setCurrentGear(0); /** gear 0 is the same as parked */
        } else if (newVelocity > 0 && newVelocity <= 20) {
            setCurrentGear(1);
        } else {
            setCurrentGear(2);
        }

        if (newVelocity > 0) {
            changeVelocity(newVelocity, getCurrentDirection());
        }
    }
}
