package com.timbuchalkacourse.section6.BankAccount;

public class BankAccount {

    private int number;
    private double balance;
    private String customerName;
    private String customerEmail;
    private int customerPhone;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public int getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(int customerPhone) {
        this.customerPhone = customerPhone;
    }

    public void deposit(int amount){
        balance += amount;
        System.out.println("You've deposited: " + amount);
        System.out.println("Current balance: " + balance);
    }

    public void withdraw(int amount){
        if (balance >= amount) {
            balance -= amount;
            System.out.println("Withdrawn "+ amount);
            System.out.println("Current balance: "+ balance);
        } else {
            System.out.println("Insufficient balance");
        }
    }
}
