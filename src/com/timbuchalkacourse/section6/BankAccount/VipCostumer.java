package com.timbuchalkacourse.section6.BankAccount;

public class VipCostumer {

    private String name;
    private int creditLimit;
    private String emailAddress;

    public VipCostumer(){
        this("Default name", 0, "default address");
    }

    public VipCostumer(String name, int creditLimit){
        this(name, creditLimit, "default address");
    }

    public VipCostumer(String name, int creditLimit, String emailAddress) {
        this.name = name;
        this.creditLimit = creditLimit;
        this.emailAddress = emailAddress;
    }

    public String getName() {
        return name;
    }

    public int getCreditLimit() {
        return creditLimit;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
}
