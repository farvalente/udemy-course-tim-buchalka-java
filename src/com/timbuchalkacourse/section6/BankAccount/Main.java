package com.timbuchalkacourse.section6.BankAccount;

public class Main {

    public static void main(String[] args) {
        BankAccount account1 = new BankAccount();

        account1.setBalance(1000);
        System.out.println(account1.getBalance());
        account1.deposit(200);
        account1.withdraw(1300);
        account1.withdraw(1200);
    }
}
