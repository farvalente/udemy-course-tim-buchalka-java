package com.timbuchalkacourse.section8.MinimumElement;

public class Main {

    public static void main(String[] args) {
        MinimumElement mE = new MinimumElement();

        int numberOfElements = mE.readInteger();
        int[] arrayOfElements = mE.readElements(numberOfElements);
        System.out.println(mE.findMin(arrayOfElements));
    }

}
