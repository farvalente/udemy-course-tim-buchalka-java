package com.timbuchalkacourse.section8.MinimumElement;

import java.util.Scanner;

public class MinimumElement {

    public int readInteger(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input number of elements: ");
        int elements = scanner.nextInt();
        return elements;
    }

    public int[] readElements(int elements){
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[elements];

        for(int i = 0 ; i < array.length; i++){
            array[i] = scanner.nextInt();
        }

        return array;
    }

    public int findMin(int[] array){

        int min = 0;

        for(int i = 0; i < array.length -1; i++){

            if(array[i] < array[i+1]) {
                min = array[i];
                array[i + 1] = array[i];
            }

            if(i == array.length - 2 && (min > array[i+1] || min ==0)){
                    min = array[i + 1];
                    break;
            }

            if(min == 0) {
                if (array[i] < array[i + 1]) {
                    min = array[i];
                    array[i + 1] = array[i];
                    continue;
                }
            }


        }
    return min;
    }
}
