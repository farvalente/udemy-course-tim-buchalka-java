package com.timbuchalkacourse.section8.ReverseArray;

import java.util.Arrays;

public class ReverseArray {

    public void reverse(int[] array) {

        int temp = 0;
        int[] reversed = new int[array.length];

        String printArray = "Array = " + Arrays.toString(array);

        if (array.length % 2 == 0) {
            for (int i = 0; i < array.length / 2; i++) {
                temp = array[i];
                reversed[i] = array[array.length - 1 - i];
                reversed[array.length - 1 - i] = temp;
            }
        } else {
            reversed[reversed.length / 2] = array[array.length / 2];
            for (int i = 0; i < array.length / 2; i++) {
                temp = array[i];
                reversed[i] = array[array.length - 1 - i];
                reversed[array.length - 1 - i] = temp;
            }
        }

        String printReversed = "Reversed array = " + Arrays.toString(reversed);

        System.out.println(printArray);
        System.out.println(printReversed);
    }


    //Another approach swapping values in the same array
    public void reverse2(int[] array) {

        int temp = 0;

        String printArray = "Array = " + Arrays.toString(array);
        System.out.println(printArray);

        for (int i = 0; i < array.length / 2; i++) {
            temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }


        printArray = "Reversed array = " + Arrays.toString(array);
        System.out.println(printArray);

    }

}
