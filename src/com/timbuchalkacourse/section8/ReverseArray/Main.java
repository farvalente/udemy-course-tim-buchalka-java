package com.timbuchalkacourse.section8.ReverseArray;


public class Main {

    public static void main(String[] args) {
        ReverseArray reverseArray = new ReverseArray();

        int[] array = new int[]{1, 2, 7 , 4, 5};

        System.out.println("Reverse 1 Method\n");

        reverseArray.reverse(array);

        System.out.println("\n****************");
        System.out.println("Reverse 2 Method\n");

        reverseArray.reverse2(array);
    }
}
