package com.timbuchalkacourse.section8.Banking;

public class Main {

    public static void main(String[] args) {

        Bank bank = new Bank("BCP");
        bank.addBranch("Leiria");
        bank.addBranch("Pombal");

        bank.addCustomer("Leiria", "Xico", 200);
        bank.addCustomer("Pombal", "Peets", 200);

        bank.addCustomerTransaction("Pombal", "Peets", 150);

        bank.listCustomers("Leiria", true);
        bank.listCustomers("Pombal", true);
    }
}
