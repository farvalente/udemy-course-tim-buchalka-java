package com.timbuchalkacourse.section8.PlayListChallenge;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {

    private String name;
    private String artist;
    private ArrayList<Song> songs;

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
        this.songs = new ArrayList<>();
    }

    public boolean addSong(String title, double duration){
        if(findSong(title) == null){
          return songs.add(new Song(title, duration));
        }
        return false;
    }

    private Song findSong(String songName){

        for (Song checkedSong : songs) {
            if(checkedSong.getTitle().equals(songName)){
                return checkedSong;
            }
        }
        return null;
    }

    public boolean addToPlayList(int trackNumber, LinkedList<Song> playList){
        int index = trackNumber -1;
        if(index >= 0 && index <= songs.size()){
            playList.add(songs.get(index));
                return true;
            }
        return false;
        }

    public boolean addToPlayList(String trackName, LinkedList<Song> playList){
        Song checkedSong = findSong(trackName);
        if(checkedSong != null){
            playList.add(checkedSong);
            return true;
        }
        return false;
    }
}

