package com.timbuchalkacourse.section8.MobilePhone;

public class Main {

    public static void main(String[] args) {
        MobilePhone mobilePhone = new MobilePhone("145455445");

        Contact contact1 = Contact.createContact("Xico", "912265148");
        Contact contact2 = Contact.createContact("Mariana", "912232333");
        Contact contact3 = Contact.createContact("Luis", "915552441");

        mobilePhone.addNewContact(contact1);
        mobilePhone.addNewContact(contact2);
        mobilePhone.addNewContact(contact1);

        mobilePhone.printContacts();

        mobilePhone.updateContact(contact1, contact3);

        mobilePhone.printContacts();

        mobilePhone.removeContact(contact3);

        mobilePhone.printContacts();
    }

    // TODO: 6/7/2022 create some methods for user inputs

}
