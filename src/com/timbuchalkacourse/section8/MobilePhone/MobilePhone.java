package com.timbuchalkacourse.section8.MobilePhone;

import java.util.ArrayList;

public class MobilePhone {

    private String myNumber;
    private ArrayList<Contact> myContacts;

    public MobilePhone(String myNumber) {
        this.myNumber = myNumber;
        myContacts = new ArrayList<>();
    }

    public boolean addNewContact(Contact addContact){
        if(findContact(addContact.getName()) >= 0){
            System.out.println("Contact already exists");
            return false;
        }
        myContacts.add(addContact);
        return true;
    }

    public boolean updateContact(Contact oldContact, Contact updatedContact){
        int foundPosition = findContact(oldContact);
        if(foundPosition < 0){
            System.out.println("Contact does not exists");
            return false;
        }

        myContacts.set(foundPosition, updatedContact);
        System.out.println(oldContact.getName() + " was replaced with " + updatedContact.getName());
        return true;
    }

    public boolean removeContact(Contact removeContact){
        if (myContacts.contains(removeContact)) {
            myContacts.remove(removeContact);
            System.out.println("Contact " + removeContact.getName() + " removed");
            return true;
        }
        System.out.println("Contact does not exists");
        return false;
    }

    private int findContact(Contact findContact){
            return this.myContacts.indexOf(findContact);
    }

    private int findContact(String findContact){
        for(int i=0; i<myContacts.size(); i++){
            Contact contact = this.myContacts.get(i);
            if(contact.getName().equals(findContact)){
                return i;
            }
        }
        return -1;
    }

    public Contact queryContact(String queryContact){
        int position = findContact(queryContact);
        if (position >= 0) {
            return this.myContacts.get(position);
        }
        return null;
    }

    public void printContacts(){

        System.out.println("Contact List:");

        for(int i = 0; i < myContacts.size(); i++){
            Contact contact = this.myContacts.get(i);
            System.out.println(i+1+". " + contact.getName() + " -> " + contact.getPhoneNumber());
        }
    }
}
