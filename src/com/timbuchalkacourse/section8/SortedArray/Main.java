package com.timbuchalkacourse.section8.SortedArray;

public class Main {

    public static void main(String[] args) {

        SortedArray sortedArray = new SortedArray();

        int[] arrayToSort = sortedArray.getIntegers(5);

        System.out.println();

        int[] arrayToPrint = sortedArray.sortIntegers(arrayToSort);
        sortedArray.printArray(arrayToPrint);
    }
}
