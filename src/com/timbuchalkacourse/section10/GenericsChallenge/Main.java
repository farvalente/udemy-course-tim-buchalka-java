package com.timbuchalkacourse.section10.GenericsChallenge;

public class Main {

    public static void main(String[] args) {

        //Create a League
        League<Team<FootballPlayer>> footballLeague = new League<>("Football League");

        //Create players of different types
        FootballPlayer xico = new FootballPlayer("Xico");
        FootballPlayer joao = new FootballPlayer("Joao");
        FootballPlayer rui = new FootballPlayer("Rui");
        FootballPlayer carlos = new FootballPlayer("Carlos");
        BasketballPlayer luis = new BasketballPlayer("Luis");

        //Create teams of different types
        Team<FootballPlayer> benfica = new Team<>("Benfica Football");
        Team<FootballPlayer> sporting = new Team<>("Sporting Football");
        Team<FootballPlayer> porto = new Team<>("Porto Football");
        Team<BasketballPlayer> benficaBasket = new Team<>("Benfica Basket");

        //Add teams to the leagues
        footballLeague.addTeam(benfica);
        footballLeague.addTeam(sporting);
        footballLeague.addTeam(porto);

        //Add players to teams, only works if they have the same type
        benfica.addPlayer(xico);
        benfica.addPlayer(joao);
        benfica.addPlayer(rui);
        benfica.addPlayer(carlos);
        //Basket player, had to be added to a basket team
        benficaBasket.addPlayer(luis);

        //Match results, only works with teams of the same type
        benfica.matchResult(porto, 2, 1);
        benfica.matchResult(sporting, 3, 0);
        sporting.matchResult(benfica, 1, 1);
        sporting.matchResult(porto, 3,1);
        porto.matchResult(benfica, 0, 3);
        porto.matchResult(sporting, 3, 3);

        footballLeague.showLeagueTable();

    }
}
