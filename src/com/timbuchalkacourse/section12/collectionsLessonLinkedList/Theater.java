package com.timbuchalkacourse.section12.collectionsLessonLinkedList;

import java.util.LinkedList;
import java.util.List;

public class Theater {

    private final String theaterName;
    public List<Seat> seats = new LinkedList<>();
    private int numRows;
    private int seatsPerRow;

    public Theater(String theaterName, int numRows, int seatsPerRow) {
        this.theaterName = theaterName;
        this.numRows = numRows;
        this.seatsPerRow = seatsPerRow;

        int lastRow = 'A' + (numRows -1);
        for(char row = 'A'; row <= lastRow; row++){
            for(int seatNum = 1; seatNum <= seatsPerRow; seatNum++){
                Seat seat = new Seat(row + String.format("%2d", seatNum));
                seats.add(seat);
            }
        }
    }

    public String getTheaterName() {
        return theaterName;
    }

    public boolean reserveSeat(String seatNumber){
        Seat requestedSeat = null;
        for(Seat seat: seats){
            if(seat.getSeatNumber().equals(seatNumber)){
                requestedSeat = seat;
                break;
            }
        }
        if (requestedSeat == null) {
            System.out.println("There's no seat " + seatNumber);
        }
        return requestedSeat.reserve();
    }

    //for testing
    public void getSeats(){
        for(Seat s : seats){
            System.out.println(s.getSeatNumber());
        }
    }

    public void printTheater(){

        int lastRow = 'A' + (numRows -1);
        for(char row = 'A'; row <= lastRow; row++){
            if (row == 'A') {
            } else {
                System.out.println("");
            }
            for(int seatNum = 1; seatNum <= seatsPerRow; seatNum++){
                System.out.print(row + String.format("%2d", seatNum) + " ");
            }
        }
    }

    public class Seat{
        private final String seatNumber;
        private boolean reserved = false;

        public Seat(String seatNumber) {
            this.seatNumber = seatNumber;
        }

        public String getSeatNumber() {
            return seatNumber;
        }

        public boolean reserve(){
            if(!reserved){
                reserved = true;
                System.out.println("Seat " + seatNumber + " reserved");
                return true;
            }
            System.out.println("Reservation failed");
            System.out.println("Seat " + seatNumber + " already reserved");

            return false;
        }

        public boolean cancel(){
            if (reserved) {
                reserved = false;
                System.out.println("Seat " + seatNumber + " reservation canceled");
                return true;
            }
            return false;
        }
    }
}
