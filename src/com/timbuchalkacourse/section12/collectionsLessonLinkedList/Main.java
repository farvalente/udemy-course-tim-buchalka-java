package com.timbuchalkacourse.section12.collectionsLessonLinkedList;

import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * this package still has work to do, for now it's just a copy of the
     * collectionsLessonArrayList package
     * @param args
     */

    public static void main(String[] args) {
        Theater theater = new Theater("D. Maria", 10, 10);
        List<Theater.Seat> seatsCopy = new ArrayList<>(theater.seats);


        //theater.getSeats();
        theater.printTheater();

        System.out.println("");
        theater.reserveSeat("A 1");
        System.out.println("");
        theater.reserveSeat("A 1");
    }

}

