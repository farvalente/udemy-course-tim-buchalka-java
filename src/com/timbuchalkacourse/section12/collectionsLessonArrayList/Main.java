package com.timbuchalkacourse.section12.collectionsLessonArrayList;

/**
 * work in progress
 */

public class Main {

    public static void main(String[] args) {
        Theater theater = new Theater("D. Maria", 10, 10);


        theater.printTheaterLayout();
        System.out.println();
        theater.reserveSeat("A1");
        theater.reserveSeat("B3");
        //theater.reserveSeat("S3");
        System.out.println();
        theater.printTheaterLayout();

        System.out.println();
        theater.cancelReservation("A1");
        System.out.println();

        theater.printTheaterLayout();

    }



}
