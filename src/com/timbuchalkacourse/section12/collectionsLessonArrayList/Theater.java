package com.timbuchalkacourse.section12.collectionsLessonArrayList;

import java.util.ArrayList;
import java.util.List;

public class Theater {

    private final String theaterName;
    private List<Seat> seats = new ArrayList<>();
    private int numRows;
    private int seatsPerRow;

    public Theater(String theaterName, int numRows, int seatsPerRow) {
        this.theaterName = theaterName;
        this.numRows = numRows;
        this.seatsPerRow = seatsPerRow;

        int lastRow = 'A' + (numRows -1);
        for(char row = 'A'; row <= lastRow; row++){
            for(int seatNum = 1; seatNum <= seatsPerRow; seatNum++){
                Seat seat = new Seat(row + "" + seatNum);
                seats.add(seat);
            }
        }
    }

    public String getTheaterName() {
        return theaterName;
    }

    public boolean reserveSeat(String seatNumber){
        Seat requestedSeat = null;
        for(Seat seat: seats){
            if(seat.getSeatNumber().equals(seatNumber)){
                requestedSeat = seat;
                break;
            }
        }
        if (requestedSeat == null) {
            System.out.println("There's no seat " + seatNumber);
        }
        return requestedSeat.reserve();
    }

        public boolean cancelReservation(String seatNumber){
            for (Seat seat : seats) {
                if (seat.getSeatNumber().equals(seatNumber) && seat.reserved) {
                    seat.cancel();
                    return true;
                }
            }
            return false;
        }

    /**
     * Method by Tim Buchalka's course
     * prints one seat per line
     */
    public void printTheater(){
        int lastRow = 'A' + (numRows -1);
        for(char row = 'A'; row <= lastRow; row++){
            if (row == 'A') {
            } else {
                System.out.println("");
            }
            for(int seatNum = 1; seatNum <= seatsPerRow; seatNum++){
                String seat = "";
                seat += row;
                seat += seatNum;
                System.out.print(seat + " ");
            }
        }
    }

    /**
     * This method prints the Theater in a grid style
     */
    public void printTheaterLayout(){

        int counter = 1;

        for(int i = 0; i < seats.size(); i++){
            if(seats.get(i).reserved){
                System.out.print(seats.get(i).getSeatRes() + " ");
            } else {
                System.out.print(seats.get(i).getSeatNumber() + " ");
            }



            if(counter != seatsPerRow){
                counter++;
            } else {
                System.out.println();
                counter = 1;
            }
        }
    }

    private class Seat{
        private final String seatNumber;
        private final String seatRes = "*R*";
        private boolean reserved = false;

        public Seat(String seatNumber) {
            this.seatNumber = seatNumber;
        }

        public String getSeatNumber() {
            return seatNumber;
        }

        public String getSeatRes() {
            return seatRes;
        }



        public boolean reserve(){
            if(!reserved){
                reserved = true;
                System.out.println("Seat " + seatNumber + " reserved.");
                return true;
            }
            System.out.println("Seat " + seatNumber + " is already reserved.");
            return false;
        }

        public boolean cancel(){
            if (reserved) {
                reserved = false;
                System.out.println("Seat " + seatNumber + " reservation canceled");
                return true;
            }
            return false;
        }
    }
}
