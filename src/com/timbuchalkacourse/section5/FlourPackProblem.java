package com.timbuchalkacourse.section5;

public class FlourPackProblem {

    public static void main(String[] args) {
        System.out.println(canPack(5, 10, 4));
    }

    public static boolean canPack(int bigCount, int smallCount, int goal){

        if(bigCount < 0 || smallCount < 0 || goal < 0 || goal > (bigCount *5) + smallCount){
            return false;
        }

        int packageWeight = 0;

        while(bigCount > 0){
            bigCount--;
            if(packageWeight + 5 > goal){
                break;
            }
            packageWeight += 5;
            if(packageWeight == goal){
                return true;
            }
        }

        if(packageWeight > goal){
            return false;
        }

        while(smallCount > 0){
            smallCount--;
            packageWeight++;
            if(packageWeight == goal){
                return true;
            }
        }

        return false;
    }
}

