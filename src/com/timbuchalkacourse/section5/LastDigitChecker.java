package com.timbuchalkacourse.section5;

public class LastDigitChecker {

    public static void main(String[] args) {
        System.out.println(isValid(9));
        System.out.println(hasSameLastDigit(10, 10, 10));

    }
     /*if (num1 < 10 || num1 > 1000 || num2 < 10 || num2 > 1000 || num3 < 10 || num3 > 1000) {
            return false;
        }*/

    public static boolean hasSameLastDigit(int num1, int num2, int num3){

        if (!isValid(num1) || !isValid(num2) || !isValid(num3)) {
            return false;
        }

        int comp1 = num1 % 10;
        int comp2 = num2 % 10;
        int comp3 = num3 % 10;

        if (comp1 == comp2 || comp1 == comp3 || comp2 == comp3) {
            return true;
        }

        return false;
    }

    public static boolean isValid(int number){

        return (number >= 10 && number <= 1000);
    }

}
