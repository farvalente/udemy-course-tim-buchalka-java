package com.timbuchalkacourse.section5;

public class PerfectNumber {

    public static void main(String[] args) {
        isPerfectNumber(6);
    }

    public static boolean isPerfectNumber(int number){

        if (number < 1) {
            return false;
        }

        int sum = 0;

        for(int i = 1; i <= number/2; i++){
            if(number % i == 0){
                sum += i;
            }
            System.out.println(sum);
        }

        if (number == sum) {
            return true;
        }
        return false;
    }

}
