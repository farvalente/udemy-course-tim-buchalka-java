package com.timbuchalkacourse.section5;

import java.util.Scanner;

public class InputCalculator {

    public static void main(String[] args) {
        inputThenPrintSumAndAverage();
    }

    public static void inputThenPrintSumAndAverage() {
        int count = 0;

        Scanner scanner = new Scanner(System.in);
        int sum = 0;

        while (scanner.hasNextInt()) {
            sum += scanner.nextInt();
            count++;
        }

        System.out.println("SUM = " + sum + " AVG = " + Math.round((double) sum / count));
        scanner.close();
    }
}
