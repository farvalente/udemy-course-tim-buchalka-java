package com.timbuchalkacourse.section5;

public class DiagonalStar {

    public static void main(String[] args) {
        printSquareStar(5);
        printSquareStar(2);
    }

    public static void printSquareStar(int numberOfRows){

        if (numberOfRows < 5) {
            System.out.println("Invalid Value");
        } else {

            for (int i = 1; i <= numberOfRows; i++) {
                String diagonalStar = "";
                for (int j = 1; j <= numberOfRows; j++) {
                    if (i == 1 || i == numberOfRows || j == 1 || j == numberOfRows || i == j || j == (numberOfRows - i + 1)) {
                        diagonalStar += "*";
                    } else {
                        diagonalStar += " ";
                    }
                }
                System.out.println(diagonalStar);
            }
        }
    }
}
