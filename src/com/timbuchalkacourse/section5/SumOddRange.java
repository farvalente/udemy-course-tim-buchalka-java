package com.timbuchalkacourse.section5;

public class SumOddRange {

    public static void main(String[] args) {
        System.out.println(isOdd(1));
        System.out.println(isOdd(2));
        System.out.println(isOdd(-1));


        System.out.println(sumOdd(0, 20));
        System.out.println(sumOdd(20, 0));
        System.out.println(sumOdd(11, 11));
        System.out.println(sumOdd(10, 10));
        System.out.println(sumOdd(-1, 2));
        System.out.println(sumOdd(1, -2));
    }

    public static boolean isOdd(int number){

        if(number < 0){
            return false;
        }

        if(number % 2 == 0){
            return true;
        }
        return false;
    }

    public static int sumOdd(int start, int end){

        if(start > end || start < 0 || end < 0){
            return -1;
        }

        int sum = 0;

        for(int i = start; i <= end; i++){
            if(isOdd(i)){
                sum += i;
            }
        }
        return sum;
    }
}
