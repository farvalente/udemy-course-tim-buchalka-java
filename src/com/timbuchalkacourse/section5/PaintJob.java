package com.timbuchalkacourse.section5;

import org.w3c.dom.ls.LSOutput;

public class PaintJob {

    public static void main(String[] args) {
        System.out.println(getBucketCount(0.75, 0.75, 0.5, 0));
    }

    public static int getBucketCount(double width, double height, double areaPerBucket, int extraBuckets){
        if (width <= 0 || height <= 0 || areaPerBucket <= 0 || extraBuckets < 0) {
            return -1;
        }

        double area = width * height;
        System.out.println(area);
        int needToBuy = (int) Math.ceil(area / areaPerBucket) - extraBuckets;

        return needToBuy;
    }


    public static int getBucketCount(double width, double height, double areaPerBucket){
        if ((width <= 0 || height <= 0 || areaPerBucket <= 0)) {
            return -1;
        }

        double area = width * height;
        int needToBuy = (int) Math.ceil(area / areaPerBucket);

        return needToBuy;
    }

    public static int getBucketCount(double area,double areaPerBucket){
        if ((area <= 0 || areaPerBucket <= 0)) {
            return -1;
        }

        int needToBuy = (int) Math.ceil(area / areaPerBucket);

        return needToBuy;
    }
}
