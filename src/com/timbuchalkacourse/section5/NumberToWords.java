package com.timbuchalkacourse.section5;

public class NumberToWords {

    public static void main(String[] args) {
        numberToWords(0);

    }

    public static void numberToWords(int number){

        if (number < 0) {
            System.out.println("Invalid Value");
        }

        int reverse = reverse(number);
        int digitCountNumber = getDigitCount(number);
        int digitCountReverse = getDigitCount(reverse);
        String word = "";

        do {
            int x = reverse % 10;
            switch (x){
                case 0:
                    word += "Zero";
                    break;
                case 1:
                    word += "One";
                    break;
                case 2:
                    word += "Two";
                    break;
                case 3:
                    word += "Three";
                    break;
                case 4:
                    word += "Four";
                    break;
                case 5:
                    word += "Five";
                    break;
                case 6:
                    word += "Six";
                    break;
                case 7:
                    word += "Seven";
                    break;
                case 8:
                    word += "Eight";
                    break;
                case 9:
                    word += "Nine";
                    break;
            }
            word += " ";
            reverse /= 10;
        } while (reverse > 0);

        if (digitCountNumber != digitCountReverse) {
            int difference = digitCountNumber - digitCountReverse;
            for(int i = 0; i < difference; i++){
                word += "Zero ";
            }
        }
        System.out.println(word);
    }

    public static int reverse(int number){

        int reverse = 0;

        while(number != 0){
            reverse += number % 10;
            number /= 10;
            if(number != 0){
                reverse *= 10;
            }
        }
        return reverse;
    }

    public static int getDigitCount(int number){

        if (number < 0) {
            return -1;
        }

        int count = 0;

        do {
            count++;
            number /= 10;
        }
        while(number != 0);

        return count;
    }
}
