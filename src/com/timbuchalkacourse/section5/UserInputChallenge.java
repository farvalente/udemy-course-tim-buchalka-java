package com.timbuchalkacourse.section5;

import java.util.Scanner;

public class UserInputChallenge {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int count = 1;
        int sum = 0;

        while (count <= 10) {
            System.out.print("Please input number " + count +": ");
            if(scanner.hasNextInt()) {
                sum += scanner.nextInt();
                count++;
            } else {
                System.out.println("Invalid Value");
            }
        }
        scanner.close();
        System.out.println("The sum of all numbers is: " + sum);
    }
}
