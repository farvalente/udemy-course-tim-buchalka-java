package com.timbuchalkacourse.section5;

public class PrimeApproach {

    public static void main(String[] args) {
        System.out.println(isNumberPrime(17));
        System.out.println(isNumberPrime(5));
        System.out.println(isNumberPrime(20));
    }

    public static boolean isNumberPrime(int number){

        int counter = 0;

        for(int i = 1; i <= number; i++){
            if(number % i == 0){
                counter++;
            }
        }

        if (counter == 2) {
            return true;
        }

        return false;
    }
}
