package com.timbuchalkacourse.section5;

public class LargestPrime {

    public static void main(String[] args) {
        System.out.println(getLargestPrime(21));
        System.out.println(getLargestPrime(217));
        System.out.println(getLargestPrime(0));
        System.out.println(getLargestPrime(7));

    }

    // TODO: 5/31/2022 this class is not working properly, have to make it work

    public static int getLargestPrime(int number){

        if(number < 1){
            return -1;
        }

        int largestPrimeFactor = 0;
        int counterForPrime = 0;

        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                int x = number / i;

                for (int j = 1; j <= x; j++) {
                    if(x % j == 0){
                        counterForPrime++;
                    }
                }


                if (counterForPrime == 2 && x > largestPrimeFactor) {
                    largestPrimeFactor = x;
                }
            }
        }
        return largestPrimeFactor;
    }

    public static boolean isNumberPrime(int number){

        int counter = 0;

        for(int i = 1; i <= number; i++){
            if(number % i == 0){
                counter++;
            }
        }

        if (counter == 2) {
            return true;
        }

        return false;
    }
}
