package com.timbuchalkacourse.section5;

public class NumberPalindrome {

    public static void main(String[] args) {
        System.out.println(isPalindrome(10));
        System.out.println(isPalindrome(-111111));
        System.out.println(isPalindrome(0));
        System.out.println(isPalindrome(1221));

    }

    public static boolean isPalindrome(int number) {

        int reverse = 0;
        int lastDigit = 0;
        int operator = number;

        if (number < 0) {
            operator = -number;
        }

        while (operator > 0) {
            lastDigit = operator % 10;
            reverse += lastDigit;
            operator /= 10;

            if (number < 0) {
                reverse = -reverse;
                if (reverse == number) {
                    return true;
                }
                reverse = -reverse;
            }
            if (reverse == number) {
                return true;
            }
            reverse *= 10;
        }
        return false;
    }

}
