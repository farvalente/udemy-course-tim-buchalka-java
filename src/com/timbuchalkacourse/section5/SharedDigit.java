package com.timbuchalkacourse.section5;

public class SharedDigit {
    public static void main(String[] args) {

    }

    public static boolean hasSharedDigit(int num1, int num2){

        if(num1 < 10 || num1 > 99 || num2 < 10 || num2 > 99){
            return false;
        }

        while (num1 > 0){
            int comparator = num1 % 10;
            int comparator2 = num2 % 10;
            if (comparator == comparator2) {
                return true;
            }
            comparator2 = num2 / 10;
            if(comparator == comparator2){
                return true;
            }
            comparator = num1 / 10;
            if(comparator == comparator2){
                return true;
            }
            int comparator3 = num2 % 10;
            if(comparator == comparator3){
                return true;
            }

            num1 /= 10;
        }

        return false;
    }
}
