package com.timbuchalkacourse.section5;

public class GreatestCommonDivisor {

    public static void main(String[] args) {
        System.out.println(getGreatestCommonDivisor(25, 15));
    }


    public static int getGreatestCommonDivisor(int first, int second){

        if (first < 10 || second < 10) {
            return -1;
        }

        int firstDivisor = 0;
        int secondDivisor = 0;
        int maximum = 0;

        for(int i = 1; i <= first; i++){
            if (first % i == 0) {
                firstDivisor = i;
            }
            for(int j = 1; j <= second; j++){
                if (second % j == 0) {
                    secondDivisor = j;
                    if (firstDivisor == secondDivisor && firstDivisor > maximum) {
                        maximum = firstDivisor;
                        break;
                    }
                }
            }
        }
        return maximum;
    }
}
