package com.timbuchalkacourse.section5;

import java.util.Scanner;

public class MinimumAndMaximumInputChallenge {

    public static void main(String[] args) {

        int count = 1;
        int minimum = Integer.MIN_VALUE;
        int maximum = 0;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Please input number " + count +": ");
        minimum = scanner.nextInt();
        count++;


        while (count <= 5) {
            System.out.print("Please input number " + count +": ");
            if(scanner.hasNextInt()) {
                int x = scanner.nextInt();

                if (x > maximum) {
                    maximum = x;
                }
                if (x < minimum) {
                    minimum = x;
                }
                count++;
            } else {
                System.out.println("Invalid Value");
            }
        }
        System.out.println("The maximum is: " + maximum);
        System.out.println("The minimun is: " + minimum);

        scanner.close();
    }


}
