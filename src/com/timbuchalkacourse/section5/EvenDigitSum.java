package com.timbuchalkacourse.section5;

public class EvenDigitSum {

    public static void main(String[] args) {

    }

    public static int getEvenDigitSum(int number){

        if (number < 0) {
            return -1;
        }

        int sum = 0;

        while (number > 0) {
            int x = number % 10;
            number /= 10;
            if (x % 2 == 0) {
                sum += x;
            }
        }
        return sum;
    }

}
