package com.timbuchalkacourse.section4;

public class PlayingCat {

    public static void main(String[] args) {

    }

    /*public static boolean isCatPlaying(boolean summer, int temperature){

        if(!summer && (temperature >= 25 && temperature <= 35)){
            return true;
        }

        if(summer && (temperature >= 25 && temperature <= 45)){
            return true;
        }

        return false;
    }*/

    public static boolean isCatPlaying(boolean summer, int temperature){

        return (!summer && (temperature >= 25 && temperature <= 35)) ||
                (summer && (temperature >= 25 && temperature <= 45));
    }
}
