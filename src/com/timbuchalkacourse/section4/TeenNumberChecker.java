package com.timbuchalkacourse.section4;

public class TeenNumberChecker {

    public static void main(String[] args) {
        System.out.println(hasTeen(20, 30, 10));
    }

    public static boolean hasTeen(int x, int y, int z){
        return (x < 20 && x > 12) || (y < 20 && y > 12) || (z < 20 && z > 12);
    }

    public static boolean isTeen(int x){
        return (x < 20 && x > 12);
    }
}
