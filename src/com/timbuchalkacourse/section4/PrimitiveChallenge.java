package com.timbuchalkacourse.section4;

public class PrimitiveChallenge {

    public static void main(String[] args) {

        byte byteVar = 120;
        short shortVar = 1000;
        int intVar = 1000000;

        long longVar = 50000 + 10 * (byteVar + shortVar + intVar);

        System.out.println(longVar);
    }
}
