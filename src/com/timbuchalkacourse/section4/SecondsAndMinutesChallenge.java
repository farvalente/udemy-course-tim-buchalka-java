package com.timbuchalkacourse.section4;

public class SecondsAndMinutesChallenge {

    public static void main(String[] args) {

        System.out.println(getDurationString(120,0));
        System.out.println(getDurationString(7200));

    }

    public static String getDurationString(int minutes, int seconds){

        int hours = minutes / 60;
        int remainder = minutes % 60;

        return hours + "h " + remainder + "m " + seconds + "s ";
    }

    public static String getDurationString(int seconds){

        if(seconds < 0){
            return "Invalid value";
        }

        int minutes = seconds / 60;
        int remainderSeconds = seconds % 60;

        return getDurationString(minutes, remainderSeconds);
    }

}
