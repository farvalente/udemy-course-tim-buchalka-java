package com.timbuchalkacourse.section4;

public class ConvertPoundToKilogram {

    public static void main(String[] args) {

        double onePound = 0.45359237;
        double givenPounds = 2;

        double poundsToKilograms = givenPounds * onePound;
        System.out.println(poundsToKilograms);
    }

}
