package com.timbuchalkacourse.section4;

public class MethodOverloadingFirstChallenge {

    public static void main(String[] args) {

        System.out.println(calcFeetAndInchesToCentimeters(10,15));
        System.out.println(calcFeetAndInchesToCentimeters(120));
    }

    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {

        //Actually this if block is not needed in this case because the check
        //is being done in the other method and the return will always return the right values
        if (feet < 0 || (inches < 0 || inches > 12)) {
            return -1;
        }

        double oneInchInCentimeters = 2.54; // One inch has 2.54 cm
        double oneFootInCentimeters = oneInchInCentimeters * 12; // One foot has 12 inches

        return (feet * oneFootInCentimeters) + (oneInchInCentimeters * inches);

    }

    public static double calcFeetAndInchesToCentimeters(double inches) {
        if (inches < 0) {
            return -1;
        }

        double inchesToFeet = inches / 12; // Because one foot has 12 inches
        double remainderInches = inches % 12;

        return calcFeetAndInchesToCentimeters(inchesToFeet, remainderInches);
    }
}
