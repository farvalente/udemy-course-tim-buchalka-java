package com.timbuchalkacourse.section4;

public class MinutesToYearsDaysCalculator {

    public static void main(String[] args) {
        printYearsAndDays(525600);
        printYearsAndDays(581760);
        printYearsAndDays(1440);
        printYearsAndDays(-1);
    }

    public static void printYearsAndDays(long minutes){

        long years = minutes / (365 * 24 * 60);
        long remainingMinutes = minutes % (365 * 24 * 60);
        long days = remainingMinutes / (24*60);

        if (minutes < 0) {
            System.out.println("Invalid Value");
        } else {

            if(remainingMinutes == 0){
                days = 0;
                System.out.println(minutes + " min = " + years + " y and " + days + " d");
            } else {
                System.out.println(minutes + " min = " + years + " y and " + days + " d");
            }
        }
    }


}
