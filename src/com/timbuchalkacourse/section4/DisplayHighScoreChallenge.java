package com.timbuchalkacourse.section4;

public class DisplayHighScoreChallenge {

    public static void main(String[] args) {

        int playerOneScore = calculateHighScorePosition(1000);
        int playerTwoScore = calculateHighScorePosition(900);
        int playerThreeScore = calculateHighScorePosition(400);
        int playerFourScore = calculateHighScorePosition(50);

        displayHighScorePosition("Xico", playerOneScore);
        displayHighScorePosition("Bis", playerTwoScore);
        displayHighScorePosition("Carlos", playerThreeScore);
        displayHighScorePosition("Braz", playerFourScore);

    }

    public static void displayHighScorePosition(String name, int position){

        System.out.println(name + " managed to get into position " + position + " on the high score table");

    }

    public static int calculateHighScorePosition(int score){

        if(score >= 1000){
            return 1;
        } else if (score >= 500){
            return 2;
        } else if (score >= 100){
            return 3;
        }
        return 4;
    }
}
