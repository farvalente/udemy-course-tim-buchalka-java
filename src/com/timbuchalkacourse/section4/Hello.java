package com.timbuchalkacourse.section4;

public class Hello {

    public static void main(String[] args) {

        System.out.println("Hello World!");

        int myFirstNumber = 5;
        System.out.println(myFirstNumber);

        float myMinFloatValue = Float.MIN_VALUE;
        float myMaxFloatValue = Float.MAX_VALUE;
        System.out.println(myMaxFloatValue);
        System.out.println(myMinFloatValue);

        double myDoubleValue = 5.25;
        float myFloatValue = 5.25f;
    }
}
