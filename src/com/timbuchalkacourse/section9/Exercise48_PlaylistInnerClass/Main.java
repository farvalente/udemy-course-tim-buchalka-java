package com.timbuchalkacourse.section9.Exercise48_PlaylistInnerClass;

import java.util.*;

public class Main {

    private static ArrayList<Album> albums = new ArrayList<>();

    public static void main(String[] args) {

        /**
         * ALBUM AT INDEX 0 IN THE ARRAY LIST
         */
        Album album = new Album("Stormbringer", "Deep Purple");
        album.addSong("Stormbringer", 4.6);
        album.addSong("Love don't mean a thing", 4.22);
        album.addSong("Holy man", 4.3);
        album.addSong("Hold on", 5.6);
        album.addSong("Lady double dealer", 3.21);
        album.addSong("You can't do it right", 6.23);
        album.addSong("High ball shooter", 4.27);
        album.addSong("The gypsy", 4.2);
        album.addSong("Soldier of fortune", 3.13);
        albums.add(album);

        /**
         * ALBUM AT INDEX 1 IN THE ARRAY LIST
         */
        album = new Album("For those about to rock", "AC/DC");
        album.addSong("For those about to rock", 5.44);
        album.addSong("I put the finger on you", 3.25);
        album.addSong("Lets go", 3.45);
        album.addSong("Inject the venom", 3.33);
        album.addSong("Snowballed", 4.51);
        album.addSong("Evil walks", 3.45);
        album.addSong("C.O.D.", 5.25);
        album.addSong("Breaking the rules", 5.32);
        album.addSong("Night of the long knives", 5.12);
        albums.add(album);

        /**
         * CREATES A PLAYLIST STORED ON A LINKED LIST
         */
        LinkedList<Song> playList = new LinkedList<>();
        albums.get(0).addToPlayList("You can't do it right", playList);
        albums.get(0).addToPlayList("Holy man", playList);
        albums.get(0).addToPlayList("Speed king", playList);  // Does not exist
        albums.get(0).addToPlayList(9, playList);
        albums.get(1).addToPlayList(8, playList);
        albums.get(1).addToPlayList(3, playList);
        albums.get(1).addToPlayList(2, playList);
        albums.get(1).addToPlayList(24, playList);

        /**
         * STARTS THE PLAYLIST
         */
        play(playList);
    }


    private static void play(LinkedList<Song> playlist) {
        Scanner scanner = new Scanner(System.in);
        boolean forward = true;
        ListIterator<Song> listIterator = playlist.listIterator();

        if (playlist.size() == 0) {
            System.out.println("No songs in playlist");
            return;
        } else {
            System.out.println("NOW PLAYING\n " +listIterator.next().toString()+"\n");
        }

        printMenu();

        while (true) {
            System.out.print("\nYour choice: \n");
            int userChoice = scanner.nextInt();

            switch (userChoice) {
                case 0:
                    System.out.println("\nCLOSING APPLICATION");
                    return;
                case 1:
                    if(!forward) {
                        if (listIterator.hasNext()) {
                            listIterator.next();
                        }
                        forward = true;
                    }

                    if(listIterator.hasNext()){
                        System.out.println("NOW PLAYING\n " +listIterator.next().toString()+"\n");
                    } else {
                        System.out.println("THIS IS THE LAST SONG OF THE PLAYLIST");
                        forward = false;
                    }
                    break;
                case 2:
                    if(forward) {
                        if(listIterator.hasPrevious()) {
                            listIterator.previous();
                        }
                        forward = false;
                    }
                    if(listIterator.hasPrevious()) {
                        System.out.println("Now playing \n".toUpperCase() + listIterator.previous().toString() +"\n");
                    } else {
                        System.out.println("We are at the start of the playlist".toUpperCase());
                        forward = true;
                    }
                    break;
                case 3:
                    if(forward) {
                        if(listIterator.hasPrevious()) {
                            System.out.println("Now replaying " + listIterator.previous().toString());
                            forward = false;
                        } else {
                            System.out.println("We are at the start of the list");
                        }
                    } else {
                        if(listIterator.hasNext()) {
                            System.out.println("Now replaying " + listIterator.next().toString());
                            forward = true;
                        } else {
                            System.out.println("We have reached the end of the list");
                        }
                    }
                    break;
                case 4:
                    printList(playlist);
                    break;
                case 5:
                    printMenu();
                    break;
                default:
                    System.out.println("Invalid Choice\n");
                    break;
            }

        }
    }

    private static void printMenu(){
        System.out.println("Available options:\nPress");
        System.out.println("0 - To Quit\n1 - Play Next Song\n2 - Play Previous Song\n3 - Replay Song\n4 - List Songs" +
                "\n5 - Print Available Actions");


    }

    private static void printList(LinkedList<Song> playlist){
        ListIterator<Song> listIterator = playlist.listIterator();
        System.out.println("=====SONGS IN THE PLAYLIST=====");
        System.out.println("===============================");
        for (Song checkedSong : playlist) {
            if(listIterator.hasNext()){
                System.out.println(checkedSong.toString());
            }
        }
        System.out.println("===============================");
    }

}
