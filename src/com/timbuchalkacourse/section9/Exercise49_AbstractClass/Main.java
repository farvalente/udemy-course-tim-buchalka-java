package com.timbuchalkacourse.section9.Exercise49_AbstractClass;

public class Main {

    public static void main(String[] args) {
        /*MyLinkedList list = new MyLinkedList(null);
        list.traverse(list.getRoot());*/

        SearchTree tree = new SearchTree(null);
        tree.traverse(tree.getRoot());

        //String stringData = "Lisboa Leiria Porto Almada Coimbra Faro Leiria Setúbal Braga Castelo-Branco";
        String stringData = "8 4 5 6 3 2 1";

        String[] data = stringData.split(" ");
        for (String s : data) {
            tree.addItem(new Node(s));
        }

        tree.traverse(tree.getRoot());

        /*list.traverse(list.getRoot());
        list.removeItem(new Node("3"));
        list.traverse(list.getRoot());

        System.out.println("Adding item");
        list.addItem(new Node("3"));
        list.traverse(list.getRoot());

        System.out.println("Adding item");
        list.addItem(new Node("3"));
        list.traverse(list.getRoot());*/
    }
}
