package com.timbuchalkacourse.section9.Exercise47_Interface;

import java.util.List;

public class Main {


    public static void main(String[] args) {

        Player player1 = new Player("Xico", 100, 10);
        Player player2 = new Player("Tim", 100, 10);

        List<String> list = player1.write();

        player1.read(list);

        System.out.println(player1.toString());


    }



}
