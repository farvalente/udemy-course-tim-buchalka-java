package com.timbuchalkacourse.section9.Exercise47_Interface;

import java.util.List;

public interface ISaveable {

    List<String> write();
    void read(List<String> list);

}
